/**
 * Enter here the user flows and custom policies for your B2C application
 * To learn more about user flows, visit: https://docs.microsoft.com/en-us/azure/active-directory-b2c/user-flow-overview
 * To learn more about custom policies, visit: https://docs.microsoft.com/en-us/azure/active-directory-b2c/custom-policy-overview
 */
const b2cPolicies = {
    names: {
        signUpSignIn: "signup_signin",
        forgotPassword: "passwordreset",
        editProfile: "profileediting"
    },
    authorities: {
        signUpSignIn: {
            authority: "https://cizb2c.b2clogin.com/cizb2c.onmicrosoft.com/B2C_1_signup_signin",
        },
        forgotPassword: {
            authority: "https://cizb2c.b2clogin.com/cizb2c.onmicrosoft.com/B2C_1_passwordreset",
        },
        editProfile: {
            authority: "https://cizb2c.b2clogin.com/cizb2c.onmicrosoft.com/B2C_1_profileediting"
        }
    },
    authorityDomain: "cizb2c.b2clogin.com"
}